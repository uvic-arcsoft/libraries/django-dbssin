# DBSSIN

DBSSIN (Database-Spreadsheet Integration) is a library designed to streamline
the ingestion of Excel spreadsheets into database schemas. It aims to reduce
manual effort and code duplication by providing a standardized approach for
mapping spreadsheet data to database models.

## Purpose

The primary purpose of DBSSIN is to simplify the ingestion process for projects
that frequently deal with Excel spreadsheets. By defining models with
spreadsheet mappings, developers can automate much of the manual work involved
in mapping spreadsheet data to database schemas. This helps reduce errors and
save time in the long run.

## Current Limitations

- DBSSIN currently only supports mapping data from Excel spreadsheets to
database models. Other spreadsheet formats such as CSV are not supported.
- The library requires explicit mapping of each field to a specific column in
the spreadsheet, which can be cumbersome for spreadsheets with a large number
of columns.
- Adding fine-grained validation beyond checking for null or blank values
within the model definition is not currently supported.

## Quick Start

1. Install the package using pip:

  ```bash
  pip install --extra-index-url=https://gitlab.com/api/v4/projects/57682673/packages/pypi/simple django-dbssin
  `````

2. Add `dbssin` to your `INSTALLED_APPS` settings:

  ```python
  INSTALLED_APPS = [
      ...
      "dbssin",
  ]
  ```

3. Import the `ingest_spreadsheet` function in your view and call it with the path to your spreadsheet:

  ```python
  import dbssin

  def upload_spreadsheet(request):
      """View for uploading a spreadsheet"""
      ctx = {} # Context dictionary
      if request.method == "POST" and request.FILES.get("file"):
          uploaded_file = request.FILES["file"]
          errors = dbssin.ingest_spreadsheet(uploaded_file) # Any errors are returned here
          ctx["errors"] = errors
          
      return render(request, "upload.html", ctx) # Do whatever you want with the errors
  ```

To use DBSSIN, define your database models as subclasses of `Model`. Each field
in the model should be mapped to a specific column in the spreadsheet using the
`mapping` parameter.

Here's an example of how you might define a `Contact` model for a basic
contacts spreadsheet:

```python
import dbssin

class Contact(Model):
    name = dbssin.CharField(..., mapping='A')
    number = dbssin.CharField(..., mapping='B')
    address = dbssin.CharField(..., mapping='C')
```

This example defines a `Contact` model with fields for name, number, and
address, mapped to columns A, B, and C respectively. While the field names in
your model don't necessarily need to match exactly with your spreadsheet, the
columns they represent must align.

If the model names match the spreadsheet names, the models will be automatically 
populated from the corresponding sheets. Otherwise, include the mixin 'SheetMixin'
into your model and update a new variable in the model called 'sheet' with the name 
of the sheet.

```python
class Writer(models.Model, dbssin.SheetMixin):
    """Model for the Writer table"""

    sheet = "Author"
```

## Additions

Optionally, if you want to automatically populate the `mapping` attribute of
your models with the column names of the spreadsheet, you can run the
following management command:

```bash
python manage.py update_mapping <spreadsheet_path>
```

`update_mapping` also accepts two optional arguments:

- `--skip-conf`, skips the confirmation prompt
- `--no-diff`, skips the diff output of your current model file, and `django_dbssin`'s proposed changes

You can also use the `update_mapping` function as you would the
`ingest_spreadsheet` function by importing it in a similar fashion.
