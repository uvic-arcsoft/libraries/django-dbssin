# Development

## Testing against different versions of Python

Different virtual environments for [current versions of
Python](https://devguide.python.org/versions/) can be created with something
like:

```
$ PYTHON=python3.11 make dev-setup
...
$ rm -f .lintcache/*
$ tests/test-all
...
$ mv venv venv-3.11
```

Then could create a symbolic link `venv` to whichever version being tested.
After the tests pass, ensure the version is listed in `pyproject.toml` and
that any no longer supported are not.
