#!/bin/bash
# 
# Run all available test suites from a single script.

# --------------------------------------------------------------------------
#                                                            configuration
# --------------------------------------------------------------------------

# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

# --------------------------------------------------------------------------
#                                                            functions
# --------------------------------------------------------------------------

fail_to()
{
  2>&1 echo Unable to $@
  exit 1
}

warn_couldnt()
{
  2>&1 echo Unable to $@
}

# --------------------------------------------------------------------------
#                                                         argument parsing
# --------------------------------------------------------------------------

usage()
{
  cat <<EOF
Usage: $(basename $0) [--no-linting --no-unit]
       $(basename $0) -h|--help

Runs available testing for this application.

Currently limited to inspection, unit and user interface testing.
EOF
}

# defaults
linting=1
unittests=1

while [ -n "$1" ]
do
  case "$1" in
    -h|--help)
      usage
      exit 0
      ;;
    --no-linting)
      linting=0
      ;;
    --no-unit)
      unittests=0
      ;;
    *)
      2>&1 echo "Unrecognized: $1"
      usage
      exit 1
      ;;
  esac
  shift
done

# --------------------------------------------------------------------------
#                                                                execution
# --------------------------------------------------------------------------

export PYTHONPATH=.:$PYTHONPATH

# linting
if (( linting ))
then
  tests/linting/intest yamllint pylint shellcheck || exit 1
fi

# unittests
if (( unittests ))
then
  python runtests.py || exit 1
fi
