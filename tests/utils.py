# pylint:
"""Utility functions for the dbssin app"""

import re
import os
from collections import defaultdict
from typing import List, Dict, Tuple

from dbssin.update_mapping import ClassPredictions

RE_RESULT = re.compile(r"^(Success|Error)\:\s*(.*)$")
RE_CELL = re.compile(r"^([A-Z]+)(\d+)\:?(\d+)?$")


def construct_file_path(file_name: str) -> str:
    """Constructs the file path for a test file"""
    return os.path.join(os.path.dirname(__file__), "test_files", file_name)


def parse_cell_range(cell_ranges: List[str]) -> Dict[str, Tuple[str, str]]:
    """Parses a cell range"""
    mapping = defaultdict(tuple)

    for cell_range in cell_ranges:
        start_letter, start_row, end_row = RE_CELL.search(cell_range).groups()
        mapping[start_letter] = (start_row, end_row)

    return mapping


def cells_in_range(cells: List[str], mapping: Dict[str, Tuple[str, str]]) -> bool:
    """Checks if a cell, or multiple cells, are contained within a range of cells"""
    for cell in cells:
        match = RE_CELL.search(cell)

        if not match:
            return False

        letter, row, _ = match.groups()
        if letter not in mapping:
            return False

        start_row, end_row = mapping[letter]
        if end_row:
            if not int(start_row) <= int(row) <= int(end_row):
                return False
        else:
            if int(start_row) != int(row):
                return False

    return True


def destructure_prediction(
    prediction: Dict[str, ClassPredictions],
) -> Dict[str, Dict[str, str]]:
    """Destructure a prediction"""
    destructured = {}
    for cls_name, cls_prediction in prediction.items():
        destructured[cls_name] = cls_prediction.predictions
    return destructured
