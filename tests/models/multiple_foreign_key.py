# pylint: disable=too-few-public-methods, too-many-ancestors
"""Model for multiple foreign key relationships"""

from django.db import models
import dbssin


class Author(models.Model):
    """Model for the Authors table"""

    name = dbssin.CharField(max_length=128, unique=True, mapping="B", db_index=True)


class Book(models.Model):
    """Model for the Books table"""

    title = dbssin.CharField(max_length=128, mapping="B", db_index=True)
    author1 = dbssin.ForeignKey(
        "Author",
        on_delete=models.CASCADE,
        mapping="C",
        related_name="author1",
    )
    author2 = dbssin.ForeignKey(
        "Author",
        on_delete=models.CASCADE,
        mapping="D",
        related_name="author2",
    )

class Writer(models.Model, dbssin.SheetMixin):
    """Model for the Writer table"""

    sheet = "Author"

    name = dbssin.CharField(max_length=128, unique=True, mapping="B", db_index=True)


class Novel(models.Model, dbssin.SheetMixin):
    """Model for the Novel table"""

    sheet = "Book"

    title = dbssin.CharField(max_length=128, mapping="B", db_index=True)
    writer1 = dbssin.ForeignKey(
        "Writer",
        on_delete=models.CASCADE,
        mapping="C",
        related_name="writer1",
    )
    writer2 = dbssin.ForeignKey(
        "Writer",
        on_delete=models.CASCADE,
        mapping="D",
        related_name="writer2",
    )
