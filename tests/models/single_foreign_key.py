# pylint: disable=too-few-public-methods, too-many-ancestors
"""Model for single foreign key relationship"""

from django.db import models
import dbssin


class Sites(models.Model):
    """Model for the Sites table"""

    dating_notes = dbssin.TextField(blank=True, mapping="H", null=True)
    card_analogue = dbssin.IntegerField(blank=True, mapping="G", null=True)
    longitude = dbssin.FloatField(mapping="B")
    site_code = dbssin.CharField(
        max_length=32, unique=True, mapping="E", db_index=True
    )
    dating_ref = dbssin.TextField(blank=True, mapping="I", null=True)
    latitude = dbssin.FloatField(mapping="A")
    region = dbssin.CharField(max_length=32, blank=True, mapping="C", null=True)
    name = dbssin.TextField(mapping="D")
    c_dates = dbssin.TextField(blank=True, mapping="F", null=True)


class ScreenSize(models.TextChoices):
    """Choices for the screen size field"""

    FINE = "FINE", "Fine"
    COARSE = "COARSE", "Coarse"


class Excavations(models.Model):
    """Model for the Excavations table"""

    site = dbssin.ForeignKey("Sites", mapping="A", on_delete=models.CASCADE)
    name = dbssin.CharField(
        max_length=64, db_index=True, mapping="B", default="Excavation"
    )
    company = dbssin.TextField(blank=True, mapping="E", null=True)
    permit = dbssin.TextField(blank=True, mapping="F", null=True)
    year = dbssin.IntegerField(blank=True, mapping="G", null=True)
    screen_size = dbssin.CharField(
        max_length=6,
        choices=ScreenSize.choices,
        blank=True,
        mapping="D",
        null=True,
    )

class Locations(models.Model, dbssin.SheetMixin):
    """Model for the Locations table"""

    sheet = "Sites"

    dating_notes = dbssin.TextField(blank=True, mapping="H", null=True)
    card_analogue = dbssin.IntegerField(blank=True, mapping="G", null=True)
    longitude = dbssin.FloatField(mapping="B")
    site_code = dbssin.CharField(
        max_length=32, unique=True, mapping="E", db_index=True
    )
    dating_ref = dbssin.TextField(blank=True, mapping="I", null=True)
    latitude = dbssin.FloatField(mapping="A")
    region = dbssin.CharField(max_length=32, blank=True, mapping="C", null=True)
    name = dbssin.TextField(mapping="D")
    c_dates = dbssin.TextField(blank=True, mapping="F", null=True)


class Projects(models.Model, dbssin.SheetMixin):
    """Model for the Projects table"""

    sheet = "Excavations"

    site = dbssin.ForeignKey("Locations", mapping="A", on_delete=models.CASCADE)
    name = dbssin.CharField(
        max_length=64, db_index=True, mapping="B", default="Project"
    )
    company = dbssin.TextField(blank=True, mapping="E", null=True)
    permit = dbssin.TextField(blank=True, mapping="F", null=True)
    year = dbssin.IntegerField(blank=True, mapping="G", null=True)
    screen_size = dbssin.CharField(
        max_length=6,
        choices=ScreenSize.choices,
        blank=True,
        mapping="D",
        null=True,
    )
