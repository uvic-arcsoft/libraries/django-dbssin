# pylint
# Reference: https://docs.djangoproject.com/en/5.0/topics/db/models/#organizing-models-in-a-package
from .single_foreign_key import Sites, Excavations, ScreenSize
from .multiple_foreign_key import Author, Book
