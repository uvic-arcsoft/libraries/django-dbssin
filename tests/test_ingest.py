# pylint: disable=no-self-use, protected-access
# protected-access is disabled to allow access to _meta attribute
"""Unit tests for the dbssin app"""

import io
import os

from openpyxl import load_workbook
from openpyxl.worksheet.worksheet import Worksheet
from django.apps import apps
from django.test import TestCase
from django.core.files.uploadedfile import InMemoryUploadedFile
import dbssin
from tests.utils import cells_in_range, parse_cell_range, construct_file_path


def construct_in_memory(
    file_path,
    content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
):
    """
    Construct InMemoryUploadedFile from a file path
    """
    # Read the file and convert to InMemoryUploadedFile
    with open(file_path, "rb") as f:
        file_content = f.read()

    # Create InMemoryUploadedFile
    file_io = io.BytesIO(file_content)
    file_name = os.path.basename(file_path)
    uploaded_file = InMemoryUploadedFile(
        file=file_io,
        field_name="file",
        name=file_name,
        content_type=content_type,
        size=len(file_content),
        charset="utf-8",
    )

    return uploaded_file

def get_field_name_for_column(model_class, column_letter):
    """
    Returns the field name corresponding to the given column letter based 
    on the 'mapping' attribute.
    """
    for field in model_class._meta.get_fields():
        # Check if the field has a 'mapping' attribute
        if hasattr(field, 'mapping') and field.mapping == column_letter.upper():
            return field.name

    return None


class TestIngest(TestCase):
    """Test the ingestion of a spreadsheet"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Initialize the models variable with all the models in the app
        self.models = list(apps.get_models())

    def get_model_class(self, model, workbook):
        """ Get the model class for the given sheet name """
        # Create a mapping of stripped sheet names to original sheet names
        original = {sheetname.strip(): sheetname for sheetname in workbook.sheetnames}
        stripped_sheetnames = original.keys()
        for sheet_name in stripped_sheetnames:
            if model.__name__ == sheet_name or (
                hasattr(model, 'sheet') and model.sheet == sheet_name
            ):
                original_sheet_name = original[sheet_name]
                return model, original_sheet_name
        return None, None

    def row_data_is_correctly_mapped_fk(self, workbook):
        """
        Ensure that each row from the spreadsheet is correctly
        mapped to a model instance and all attributes match their respective cells.
        """
        for model in self.models:
            model_class, sheet_name = self.get_model_class(model, workbook)
            if model_class is None:
                continue

            sheet: Worksheet = workbook[sheet_name]
            model_instances = list(model_class.objects.order_by("id"))

            # Get the range of valid rows
            start, end = dbssin.check_sheet(sheet, sheet_name.strip())

            # Check that each row has a corresponding model instance
            for row_idx, row in enumerate(sheet.iter_rows(min_row=start, max_row=end), start=2):
                self.assertLess(
                    row_idx - start, len(model_instances), f"Missing instance for row {row_idx}"
                )
                instance = model_instances[row_idx - start]

                # Compare each cell value to the corresponding model attribute
                for cell in row:
                    field_name = get_field_name_for_column(
                        model_class, cell.column_letter
                    )
                    if field_name is None:
                        continue

                    # Fetch the model's attribute value
                    model_value = getattr(instance, field_name, None)

                    # Foreign key fields reference the correct related model instance
                    if (str(model_value) == f"Sites object ({row_idx - 1})"
                        or str(model_value) == f"Author object ({str(cell.value).strip()})"
                        or str(model_value) == f"Locations object ({row_idx - 1})"
                        or str(model_value) == f"Writer object ({str(cell.value).strip()})"):
                        continue

                    self.assertEqual(
                        str(cell.value).strip() if cell.value is not None else "",
                        str(model_value).strip() if model_value is not None else "",
                        f"Value mismatch in row {row_idx}, column {cell.column_letter}",
                    )

    def test_single_fk_success(self):
        """
        Test to see if a spreadsheet with a single foreign key can be ingested 
        successfully regardless if the sheet name is the same as the model name
        """
        fp = construct_file_path(file_name="Minimum_Test_SingleFK.xlsx")
        uploaded_file = construct_in_memory(file_path=fp)
        errors = dbssin.ingest_spreadsheet(uploaded_file=uploaded_file)
        self.assertEqual(errors, {})

        workbook = load_workbook(fp)
        self.row_data_is_correctly_mapped_fk(workbook)

    def test_single_fk_error(self):
        """
        Test to see if a spreadsheet with a single foreign key with
        errors can be ingested and the errors are caught
        """
        fp = construct_file_path(file_name="Minimum_Test_SingleFK_Errors.xlsx")
        uploaded_file = construct_in_memory(file_path=fp)
        errors = dbssin.ingest_spreadsheet(uploaded_file=uploaded_file)
        self.assertNotEqual(errors, {})

        # Range of cells with errors
        mapping = parse_cell_range(["A4", "B4", "C2:24"])

        for _ in errors.values():
            for error_message in _:
                error, cells = error_message.split(": ")
                cells = cells.split(", ")

                self.assertEqual(error, "Invalid empty cells")
                self.assertTrue(cells_in_range(cells, mapping))

    def test_multiple_fk_success(self):
        """
        Test to see if a spreadsheet with multiple foreign keys can be ingested 
        successfully regardless if the sheet name is the same as the model name
        """
        fp = construct_file_path(file_name="Minimum_Test_MultipleFK.xlsx")
        uploaded_file = construct_in_memory(file_path=fp)
        errors = dbssin.ingest_spreadsheet(uploaded_file=uploaded_file)
        self.assertEqual(errors, {})

        workbook = load_workbook(fp)
        self.row_data_is_correctly_mapped_fk(workbook)
