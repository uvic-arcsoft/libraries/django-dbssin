# # pylint: disable=no-self-use
# """Unit tests for updating the mapping field of our mixin class"""

# from django.test import TestCase

# from tests.utils import construct_file_path, destructure_prediction
# import dbssin


# class TestUpdateMappingCol(TestCase):
#     """Test the ingestion of a spreadsheet"""

#     def test_single_fk_field_predictions(self):
#         """
#         Test to see if a spreadsheet with a single foreign key can
#         have its fields predicted correctly
#         """
#         file_name = "Minimum_Test_SingleFK.xlsx"
#         fp = construct_file_path(file_name=file_name)

#         actual = dbssin.update_mapping(uploaded_file=fp, no_diff=True, conf_callback=None)

#         expectation = {
#             "Sites": {
#                 "latitude": "A",
#                 "longitude": "B",
#                 "region": "C",
#                 "name": "D",
#                 "site_code": "E",
#                 "c_dates": "F",
#                 "card_analogue": "G",
#                 "dating_notes": "H",
#                 "dating_ref": "I",
#             },
#             "Excavations": {
#                 "site": "A",
#                 "name": "B",
#                 "screen_size": "D",
#                 "company": "E",
#                 "permit": "F",
#                 "year": "G",
#             },
#         }

#         prediction = destructure_prediction(prediction=actual[file_name].classes)
#         self.assertEqual(prediction, expectation)

#     def test_multiple_fk_field_predictions(self):
#         """
#         Test to see if a spreadsheet with multiple foreign keys can
#         have its fields predicted correctly
#         """
#         file_name = "Minimum_Test_MultipleFK.xlsx"
#         fp = construct_file_path(file_name=file_name)

#         actual = dbssin.update_mapping(uploaded_file=fp, no_diff=True, conf_callback=None)

#         expectation = {
#             "Author": {"name": "B"},
#             "Book": {"title": "B", "author1": "C", "author2": "D"},
#         }

#         prediction = destructure_prediction(prediction=actual[file_name].classes)

#         self.assertEqual(prediction, expectation)
