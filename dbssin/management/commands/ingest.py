# pylint: disable=no-member
"""Management command to ingest an Excel spreadsheet into the database"""

import os

from django.core.management.base import BaseCommand, CommandError
from dbssin.ingest import ingest_spreadsheet


class Command(BaseCommand):
    """Command class to handle the ingestion of an Excel spreadsheet"""

    help = "Ingest an Excel spreadsheet into the database"

    def add_arguments(self, parser):
        """Add arguments to the command"""
        parser.add_argument("file", type=str)

    def handle(self, *args, **options):
        """Handle the ingestion of the spreadsheet"""
        file = options["file"]
        if not os.path.exists(file):
            raise CommandError(f"File {file} does not exist")

        try:
            errors = ingest_spreadsheet(uploaded_file=file)
            if errors:
                self.stdout.write(self.style.ERROR(f"Error: {errors}"))
            else:
                self.stdout.write(
                    self.style.SUCCESS("Success: File ingested successfully")
                )
        except Exception as e:
            raise CommandError(e) from e
