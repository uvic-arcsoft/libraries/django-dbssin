# pylint: disable=no-member
"""Management command to update mapping fields for models based on an uploaded file"""

import os

from django.core.management.base import BaseCommand, CommandError

from dbssin.update_mapping import update_mapping
from dbssin.utils import has_mapping, is_mapping_updated


class Command(BaseCommand):
    """Command class to handle updating mapping attributes for models based on an uploaded file"""

    help = "Update the mapping attribute for models based on an uploaded file"

    def add_arguments(self, parser):
        """Add arguments to the command"""
        parser.add_argument(
            "file", type=str, help="The file to be updated"
        )
        parser.add_argument(
            "--skip-conf", action="store_true", help="Skip confirmation"
        )
        parser.add_argument(
            "--no-diff", action="store_true", help="Do not print the diff"
        )

    def handle(self, *args, **kwargs):
        """Handle the updating of mapping attributes"""

        def conf_callback(mf):
            """Confirmation callback"""
            answer = input(f"Update {mf.name}? (y/n) ")
            return answer.lower() == "y"

        file = kwargs["file"]
        if not os.path.exists(file):
            raise CommandError(f"File {file} does not exist")

        try:
            runtime_metrics = update_mapping(
                uploaded_file=file,
                no_diff=kwargs["no_diff"],
                conf_callback=None if kwargs["skip_conf"] else conf_callback,
            )

            file_name = os.path.basename(file)

            if is_mapping_updated(file_name, runtime_metrics):
                self.stdout.write(
                    self.style.SUCCESS(
                        (
                            "Success: Updated mapping attribute(s) for "
                            f"{', '.join(runtime_metrics[file_name].classes.keys())}. "
                        )
                    )
                )
            elif has_mapping(file_name, runtime_metrics):
                self.stdout.write(
                    self.style.WARNING(
                        (
                            "Warning: Mapping attribute(s) already exist in "
                            f"{runtime_metrics[file_name].name}"
                        )
                    )
                )
            else:
                self.stderr.write(self.style.ERROR("Error: No mappings attribute(s) updated"))
        except Exception as e:
            raise CommandError(e) from e
