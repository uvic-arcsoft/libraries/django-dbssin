# pylint:
"""Update the mapping fields for models based on an uploaded file"""

import difflib
import inspect
from pathlib import Path
from collections import defaultdict
from typing import List, Dict, Tuple, Callable
from difflib import SequenceMatcher
from dataclasses import dataclass, field

import libcst as cst
import libcst.matchers as m
from libcst.metadata import PositionProvider
from openpyxl import load_workbook
from openpyxl.workbook.workbook import Workbook
from django.conf import settings
from django.db import models
import dbssin


# Minimum acceptable ratio for a field name to be considered a match
THRESHOLD = 0.45


@dataclass
class ClassPredictions:
    """Dataclass to store the predictions for a class"""

    predictions: Dict[str, str] = field(default_factory=dict)


@dataclass
class FileMetrics:
    """Dataclass to store the metrics for a file"""

    name: str = ""
    updated: bool = False
    has_mapping: bool = True
    classes: Dict[str, ClassPredictions] = field(default_factory=dict)


# Debug metrics to keep track of file state
runtime_metrics: Dict[str, FileMetrics] = defaultdict(FileMetrics)


class Transformer(cst.CSTTransformer):
    """Transformer class to update the mapping fields for models"""

    METADATA_DEPENDENCIES = (PositionProvider,)

    def __init__(
        self,
        field_types: List[str],
        column_mapping: Dict[str, Dict[str, str]],
        excel_file_name: str,
        model_file_name: str,
    ):
        # Stack to keep track of the current class definition
        self.stack: List[Tuple[str, ...]] = []

        # All possible Django field types for a model
        self.field_types = field_types

        # Mapping of column letters to header names
        self.column_mapping = column_mapping

        # File name of the uploaded Excel file
        self.excel_file_name = excel_file_name

        # File name of the model file
        self.model_file_name = model_file_name

    def visit_ClassDef(self, node: cst.ClassDef):
        """Pushes the class name to the stack when visiting a class definition"""
        # Only consider true Model classes, i.e., skip TextChoices, etc.
        if self._is_model_class(node):
            self.stack.append(node.name.value)

    def leave_ClassDef(
        self, original_node: cst.ClassDef, updated_node: cst.ClassDef
    ) -> cst.CSTNode:
        """Pops the stack when leaving a class definition"""
        if self._is_model_class(original_node):
            self.stack.pop()
        return updated_node

    def leave_Assign(
        self, original_node: cst.Assign, updated_node: cst.Assign
    ) -> cst.BaseStatement:
        """Update the mapping field for a field assignment"""
        if self.stack and self._is_field_assignment(original_node):
            new_args = list(original_node.value.args)
            field_name = original_node.targets[0].target.value

            # When there is no best match, return the updated node as is.
            # This is done to prevent the mapping from being added to the
            # field assignment
            best_match = self._find_best_match(field_name)
            if not best_match:
                return updated_node

            current_class = self.stack[-1]
            file_metrics = runtime_metrics[self.excel_file_name]
            file_metrics.name = self.model_file_name

            if current_class not in file_metrics.classes:
                file_metrics.classes[current_class] = ClassPredictions()

            file_metrics.classes[current_class].predictions[field_name] = best_match

            # Could put this in the if statement above, but doing so prevents
            # the runtime_metrics from being updated
            if not self._is_mapping_assigned(original_node):
                # Adds mapping as the second-to-last argument in the field assignment
                # Example update:
                # Before: screen_size = dbssin.CharField(
                #             max_length=6,
                #             blank=True,
                #             null=True,
                #         )
                # After: screen_size = dbssin.CharField(
                #             max_length=6,
                #             blank=True,
                #             mapping="D",
                #             null=True,
                #         )

                mapping_arg = cst.Arg(
                    value=cst.SimpleString(f'"{best_match}"'),
                    keyword=cst.Name("mapping"),
                    equal=cst.AssignEqual(
                        whitespace_before=cst.SimpleWhitespace(value=""),
                        whitespace_after=cst.SimpleWhitespace(value=""),
                    ),
                    # Fetch the styling of the comma from the original node
                    comma=new_args[0].comma if new_args else None,
                )

                # Insert the mapping argument before the last argument
                new_args.insert(-1, mapping_arg)

                new_call = original_node.value.with_changes(args=new_args)

                file_metrics.has_mapping = False

                return updated_node.with_changes(value=new_call)

        return updated_node

    def _find_best_match(self, field_name: str) -> str:
        """Find the best match for a field name in the column mapping"""
        best_match, best_ratio = None, 0

        sheet = self.column_mapping.get(self.stack[-1], {})
        for header, column in sheet.items():
            if not header:
                continue

            # Convert the field to a form similar to that of a regular variable
            # name, assuming that the field is formatted in snake_case
            normalized_header = header.lower().strip().replace(" ", "_")

            ratio = SequenceMatcher(None, field_name, normalized_header).ratio()
            if ratio > THRESHOLD and ratio > best_ratio:
                best_match, best_ratio = column, ratio

        return best_match

    def _is_field_assignment(self, node: cst.Assign) -> bool:
        """Check if the node is an assignment to a field"""
        return m.matches(
            node,
            m.Assign(
                value=m.Call(
                    func=m.OneOf(
                        m.Attribute(
                            value=m.Name(),
                            attr=m.Name(
                                m.MatchIfTrue(lambda name: name in self.field_types)
                            ),
                        ),
                        m.Name(m.MatchIfTrue(lambda name: name in self.field_types)),
                    )
                )
            ),
        )

    @staticmethod
    def _is_mapping_assigned(node: cst.Assign) -> bool:
        """Checks to see if there is mapping already assigned"""
        return any(
            m.matches(arg, m.Arg(keyword=m.Name("mapping")))
            for arg in node.value.args
        )

    @staticmethod
    def _is_model_class(node: cst.ClassDef) -> bool:
        """Check if the node is a class definition that inherits from Model"""
        return m.matches(
            node,
            m.ClassDef(
                bases=[
                    m.Arg(
                        value=m.OneOf(
                            m.Name(
                                value="Model",
                            ),
                            m.Attribute(
                                value=m.Name(
                                    value="models",
                                ),
                                attr=m.Name(
                                    value="Model",
                                ),
                            ),
                        )
                    )
                ]
            ),
        )


def get_column_mapping(wb: Workbook) -> Dict[str, str]:
    """Returns a mapping of column letters to header names"""
    # Example mapping:
    # {
    #   "Sites": {"Lat": "A", "Long": "B", ...},
    #   "Excavations": {"Site #": "A", "Name": "B", ...},
    # }
    mapping = defaultdict(dict)
    for sheet in wb.sheetnames:
        ws = wb[sheet]

        # Assuming the first row contains the headers
        for row in ws.iter_rows(max_row=1):
            for cell in row:
                mapping[sheet][cell.value] = cell.column_letter

    return mapping


def get_all_field_types() -> List[str]:
    """Returns a list of all field types in the models module"""
    field_types = []
    for name, obj in inspect.getmembers(dbssin.mixins):
        # Skip the Field class itself
        if (
            inspect.isclass(obj)
            and issubclass(obj, models.Field)
            and obj is not models.Field
        ):
            field_types.append(name)

    return field_types


def get_model_files() -> List[str]:
    """Returns a list of all available model files"""
    # Recursively fetch all model file paths either in the root models.py file
    # or in the models subdirectory, excluding the __init__.py file
    model_files = [
        path
        for pattern in ("models.py", "models/*.py")
        for path in Path(settings.BASE_DIR).rglob(pattern)
        if "__init__" not in path.name
    ]

    return model_files


def update_mapping(
    uploaded_file: str,
    no_diff: bool = False,
    conf_callback: Callable[[Path], bool] = None,
) -> Dict[str, FileMetrics]:
    """Update the mapping fields for models based on an uploaded file"""
    excel_file_name = Path(uploaded_file).name

    # Clear the debug metrics before starting
    runtime_metrics.clear()

    wb = load_workbook(uploaded_file)
    column_mapping = get_column_mapping(wb)
    field_types = get_all_field_types()
    model_files = get_model_files()

    for mf in model_files:
        # Get the source code from the model file
        content = mf.read_text()

        # Parse the source code into an CST
        node = cst.parse_module(content)

        # Mainly used for debugging purposes as it provides additional information
        # about the node, such as the line number
        # See: https://libcst.readthedocs.io/en/latest/metadata.html for more details
        wrapped_node = cst.MetadataWrapper(node)

        # Transform the CST to update the mapping fields
        transformed_node = wrapped_node.visit(
            Transformer(
                field_types, column_mapping, excel_file_name, model_file_name=mf.name
            )
        )

        # Generate a diff between the original and transformed code
        diff = "".join(
            difflib.unified_diff(
                content.splitlines(1), transformed_node.code.splitlines(1)
            )
        )

        if not diff:
            continue

        if not no_diff:
            print(diff)

        if conf_callback and not conf_callback(mf):
            continue

        # Write the transformed code back to the model file
        mf.write_text(transformed_node.code)

        runtime_metrics[excel_file_name].updated = True

    return runtime_metrics
