# pylint: disable=too-many-ancestors, too-few-public-methods
#         too-many-ancestors: inheriting from Django's model classes
#         and we have no control over ancestors or depth of inheritance
#         too-few-public-methods: Mixin classes are not expected to
#         have many public methods
"""Module defines a mixin and several Django model fields that use the mixin"""

from django.db import models


class SheetMixin:
    """Mixin that adds a sheet attribute to a Django model"""

    def __init__(self, *args, sheet=None, **kwargs):
        self._sheet = sheet
        super().__init__(*args, **kwargs)

    @property
    def sheet(self):
        """Getter for sheet"""
        return self._sheet

    @sheet.setter
    def sheet(self, value):
        """Setter for sheet"""
        self._sheet = value


class MappingMixin:
    """Mixin that adds a mapping attribute to a Django model field"""

    def __init__(self, *args, mapping=None, **kwargs):
        self._mapping = mapping
        super().__init__(*args, **kwargs)

    @property
    def mapping(self):
        """Getter for mapping"""
        return self._mapping

    @mapping.setter
    def mapping(self, value):
        """Setter for mapping"""
        self._mapping = value


# List of all the Django fields that can be mapped
class CharField(MappingMixin, models.CharField):
    """CharField augmented with a mapping attribute"""

class IntegerField(MappingMixin, models.IntegerField):
    """IntegerField augmented with a mapping attribute"""

class EmailField(MappingMixin, models.EmailField):
    """EmailField augmented with a mapping attribute"""

class FloatField(MappingMixin, models.FloatField):
    """FloatField augmented with a mapping attribute"""

class AutoField(MappingMixin, models.AutoField):
    """AutoField augmented with a mapping attribute"""

class BigAutoField(MappingMixin, models.BigAutoField):
    """BigAutoField augmented with a mapping attribute"""

class BigIntergerField(MappingMixin, models.BigIntegerField):
    """BigIntegerField augmented with a mapping attribute"""

class BinaryField(MappingMixin, models.BinaryField):
    """BinaryField augmented with a mapping attribute"""

class BooleanField(MappingMixin, models.BooleanField):
    """BooleanField augmented with a mapping attribute"""

class CommaSeparatedIntegerField(MappingMixin, models.CommaSeparatedIntegerField):
    """CommaSeparatedIntegerField augmented with a mapping attribute"""

class DateField(MappingMixin, models.DateField):
    """DateField augmented with a mapping attribute"""

class DateTimeField(MappingMixin, models.DateTimeField):
    """DateTimeField augmented with a mapping attribute"""

class DecimalField(MappingMixin, models.DecimalField):
    """DecimalField augmented with a mapping attribute"""

class DurationField(MappingMixin, models.DurationField):
    """DurationField augmented with a mapping attribute"""

class FileField(MappingMixin, models.FileField):
    """FileField augmented with a mapping attribute"""

class FilePathField(MappingMixin, models.FilePathField):
    """FilePathField augmented with a mapping attribute"""

class ForeignObject(MappingMixin, models.ForeignObject):
    """ForeignObject augmented with a mapping attribute"""

class GeneratedField(MappingMixin, models.GeneratedField):
    """GeneratedField augmented with a mapping attribute"""

class GenericIPAddressField(MappingMixin, models.GenericIPAddressField):
    """GenericIPAddressField augmented with a mapping attribute"""

class IPAddressField(MappingMixin, models.IPAddressField):
    """IPAddressField augmented with a mapping attribute"""

class ImageField(MappingMixin, models.ImageField):
    """ImageField augmented with a mapping attribute"""

class JSONField(MappingMixin, models.JSONField):
    """JSONField augmented with a mapping attribute"""

class NullBooleanField(MappingMixin, models.NullBooleanField):
    """NullBooleanField augmented with a mapping attribute"""

class PositiveBigIntergerField(MappingMixin, models.PositiveIntegerField):
    """PositiveBigIntegerField augmented with a mapping attribute"""

class PositiveIntegerField(MappingMixin, models.PositiveIntegerField):
    """PositiveIntegerField augmented with a mapping attribute"""

class PositiveSmallIntegerField(MappingMixin, models.PositiveSmallIntegerField):
    """PositiveSmallIntegerField augmented with a mapping attribute"""

class SlugField(MappingMixin, models.SlugField):
    """SlugField augmented with a mapping attribute"""

class SmallAutoField(MappingMixin, models.SmallAutoField):
    """SmallAutoField augmented with a mapping attribute"""

class SmallIntegerField(MappingMixin, models.SmallIntegerField):
    """SmallIntegerField augmented with a mapping attribute"""

class TextField(MappingMixin, models.TextField):
    """TextField augmented with a mapping attribute"""

class TimeField(MappingMixin, models.TimeField):
    """TimeField augmented with a mapping attribute"""

class URLField(MappingMixin, models.URLField):
    """URLField augmented with a mapping attribute"""

class UUIDField(MappingMixin, models.UUIDField):
    """UUIDField augmented with a mapping attribute"""

class ForeignKey(MappingMixin, models.ForeignKey):
    """ForeignKey augmented with a mapping attribute"""
    def __init__(self, to, mapping=None, **kwargs):
        super().__init__(to, mapping=mapping, **kwargs)

class ManyToManyField(MappingMixin, models.ManyToManyField):
    """ManyToManyField augmented with a mapping attribute"""

class OneToOneField(MappingMixin, models.OneToOneField):
    """OneToOneField augmented with a mapping attribute"""

class OrderWrt(MappingMixin, models.OrderWrt):
    """OrderWrt augmented with a mapping attribute"""
