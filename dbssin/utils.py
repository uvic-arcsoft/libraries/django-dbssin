# pylint:
"""Utility functions for the dbssin app"""

from typing import Dict

from dbssin.update_mapping import FileMetrics


def is_mapping_updated(
    file_name: str, runtime_metrics: Dict[str, FileMetrics]
) -> bool:
    """Check if a file has been updated"""
    return runtime_metrics[file_name].updated


def has_mapping(file_name: str, runtime_metrics: Dict[str, FileMetrics]) -> bool:
    """Check if a file has a mapping attribute"""
    return runtime_metrics[file_name].has_mapping


def has_all_predictions(
    file_name: str, runtime_metrics: Dict[str, FileMetrics]
) -> bool:
    """Check if a file has predictions"""
    for model in runtime_metrics[file_name].classes:
        if not runtime_metrics[file_name].classes[model]:
            return False

    return True
