# pylint: disable=invalid-name, protected-access, global-statement, undefined-loop-variable, too-few-public-methods
"""File ingestion logic for the dbssin app"""

from statistics import mode
from collections import defaultdict
from typing import List, Dict, Optional, Type, Tuple, Set

from openpyxl import load_workbook
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from django.apps import apps
from django.db.utils import IntegrityError
from django.db.models.fields.related import ForeignKey
from django.db.models.base import ModelBase
from django.db import transaction
from django.core.files.uploadedfile import InMemoryUploadedFile


# Maximum number of non-empty columns we consider to determine the
# probable start of the header row
MAX_BREADTH = 10

preconditions: Dict[str, Dict[str, List[str]]] = {}


class ErrorManager:
    """Singleton class for managing errors"""

    _instance = None
    errors = Dict[str, List[str]]

    def __new__(cls):
        """Creates a new instance of the ErrorManager class if it doesn't exist"""
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance.errors = defaultdict(list)
        return cls._instance

    def add_error(self, model_name, error_message):
        """Adds an error to the errors dictionary"""
        self.errors[model_name].append(error_message)

    def get_errors(self):
        """Returns the errors dictionary"""
        return self.errors


class ForeignKeyInfo:
    """Information about a foreign key"""

    def __init__(
        self,
        related_model: Type[ModelBase],
        field: str,
        mapping: str,
        name: str,
    ) -> None:
        """Initializes the ForeignKeyInfo object"""
        self.related_model = related_model
        self.field = field
        self.mapping = mapping
        self.name = name


def allowed_empty_fields(Model: Type[ModelBase]) -> Set[str]:
    """Returns a set of fields as column letters that are allowed to be empty for a given model"""
    return {
        field.mapping
        for field in Model._meta.fields
        if (field.blank and field.null) and field.mapping
    }


def considered_fields(Model: Type[ModelBase]) -> Set[str]:
    """Returns a set of fields as column letters that are considered for a given model"""
    return {
        field.mapping
        for field in Model._meta.fields
        if hasattr(field, "mapping") and field.mapping
    }


def create_column_mapping(Model: Type[ModelBase]) -> Dict[str, str]:
    """Creates a mapping of column letters to field names for a given model"""
    return {
        field.mapping: field.name
        for field in Model._meta.fields
        if hasattr(field, "mapping") and field.mapping
    }


def get_foreign_keys(Model: Type[ModelBase]) -> List[Optional[ForeignKeyInfo]]:
    """Returns a list of foreign key for a given model or None if there are no foreign keys"""
    fks: List[ForeignKeyInfo] = []
    for field in Model._meta.fields:
        if isinstance(field, ForeignKey):
            fks.append(
                ForeignKeyInfo(
                    related_model=field.related_model,
                    field=field.name,
                    mapping=field.mapping,
                    name=field.name,
                )
            )

    return fks


def get_related_field(value: str, fk: ForeignKeyInfo) -> Optional[str]:
    """Returns the field in the related model that corresponds to the given value"""
    for obj in fk.related_model.objects.all().values():
        for k, v in obj.items():
            if v == value:
                return k
    return None


def get_header_start(sheet: Worksheet) -> int:
    """Returns the row number where the header most probably starts"""
    candidates = []

    for col in sheet.iter_cols():
        if len(candidates) == MAX_BREADTH:
            break

        first_non_empty = next((cell.row for cell in col if cell.value), None)

        if first_non_empty:
            candidates.append(first_non_empty)

    # Return the most common row number upto MAX_BREADTH
    return mode(candidates[:MAX_BREADTH] or [0])


def align_columns(row: Dict[str, str | int], mapping: Dict[str, str]) -> Dict[str, str]:
    """
    Given a mapping of column letters to field names and a row of data, aligns the columns
    to the fields in the model
    """
    return {
            mapping[key]: row[key]
            for key in mapping.keys()
        }


@transaction.atomic
def process_row(
    row: Tuple[str | int, ...],
    mapping: Dict[str, str],
    Model: Type[ModelBase],
) -> None:
    """Process a single row of data"""

    def handle_foreign_keys(
        aligned_row: Dict[str, str], fks: List[ForeignKeyInfo]
    ) -> None:
        """Handles rows with foreign keys"""
        try:
            for fk in fks:
                fk_value = aligned_row.pop(fk.name)

                if not (related_field := get_related_field(fk_value, fk)):
                    raise AttributeError(
                        f"ForeignKey value {fk_value} not found in related model {fk.related_model}"
                    )

                aligned_row[fk.field] = fk.related_model.objects.get(
                    **{related_field: fk_value}
                )

            Model(**aligned_row).save()
        except (IntegrityError, AttributeError) as e:
            ErrorManager().add_error(current_model, str(e))

    def handle_no_foreign_keys(aligned_row: Dict[str, str]) -> None:
        """Handles rows with no foreign keys"""
        try:
            Model(**aligned_row).save()
        except IntegrityError as e:
            ErrorManager().add_error(current_model, str(e))

    current_model = Model.__name__

    row_data: Dict[str, str | int] = {}
    for cell in row:
        row_data[cell.column_letter] = cell.value

    aligned_row = align_columns(row_data, mapping)

    if fks := get_foreign_keys(Model):
        handle_foreign_keys(aligned_row, fks)
    else:
        handle_no_foreign_keys(aligned_row)


def process_sheet(
    Model: Type[ModelBase], sheet: Worksheet, start: int, end: int
) -> None:
    """Processes a single sheet of the spreadsheet"""
    mapping = create_column_mapping(Model)
    for row in sheet.iter_rows(min_row=start, max_row=end):
        process_row(row, mapping, Model)


def check_sheet(sheet: Worksheet, current_model: str) -> Tuple[int, int]:
    """Checks to see if a sheet is valid"""
    # OpenPyXL is 1-indexed so this value is impossible
    start, end = 0, 0

    def is_row_valid(row_data: Dict[str, str | int], pos: int) -> bool:
        """
        Checks if a row is valid. A row is valid if it has at least one
        non-empty cell and all the empty cells are allowed to be empty
        """
        allowed_empty = preconditions[current_model]["allowed_empty"]
        considered = preconditions[current_model]["considered"]

        empty, non_empty = set(), set()
        for k, v in row_data.items():
            # Skip fields that don't have a corresponding mapping field in the model
            if k not in considered:
                continue

            if v in (None, ""):
                empty.add(k)
            else:
                non_empty.add(k)

        # The entire row is empty
        if not non_empty:
            return False

        # There are fields that are not allowed to be empty
        invalid_empty = empty - allowed_empty
        if invalid_empty:
            cells = [f"{cell.upper()}{pos}" for cell in invalid_empty]
            ErrorManager().add_error(
                current_model, f"Invalid empty cells: {', '.join(cells)}"
            )

            return False

        return True

    row_data: Dict[str, str | int] = {}

    # Get the row number where the header starts
    if (sheet_header_start := get_header_start(sheet)) == 0:
        ErrorManager().add_error(current_model, "Could not determine the header row")
        return (start, end)

    # Start at 1 to be consistent with OpenPyXL's 1-indexing
    for i, row in enumerate(sheet.iter_rows(), start=1):
        for cell in row:
            row_data[cell.column_letter] = cell.value

        if is_row_valid(row_data, pos=i):
            # If the start has not been set, and the current row is not the header row
            if not start and cell.row != sheet_header_start:
                start = i
            end = i

    return (start, end)


def ingest_spreadsheet(uploaded_file: InMemoryUploadedFile) -> Dict[str, set]:
    """Ingests a spreadsheet into the database, given the uploaded file"""
    # Initialize the MODELS variable with all the models in the app
    MODELS: List[Type[ModelBase]] = list(apps.get_models())

    # Clear the errors dictionary, to avoid any spill-over between runs
    ErrorManager().errors.clear()

    global preconditions

    workbook: Workbook = load_workbook(uploaded_file)

    # Create a mapping of stripped sheet names to original sheet names
    original = {sheetname.strip(): sheetname for sheetname in workbook.sheetnames}
    stripped_sheetnames = original.keys()

    considering: List[Type[ModelBase]] = [
        Model for Model in MODELS
        if (hasattr(Model, 'sheet') and Model.sheet in stripped_sheetnames)
        or Model.__name__ in stripped_sheetnames
    ]

    # Initialize the preconditions variable that holds the fields that
    # are allowed to be empty for each model, and the fields that are
    # considered for each model
    preconditions = {
        Model.__name__: {
            "allowed_empty": allowed_empty_fields(Model),
            "considered": considered_fields(Model),
        }
        for Model in considering
    }

    for Model in considering:
        sheet_name = getattr(Model, 'sheet', None) or Model.__name__
        sheet = workbook[original[sheet_name]]
        start, end = check_sheet(sheet, current_model=Model.__name__)
        if ErrorManager().get_errors():
            continue

        process_sheet(Model, sheet, start, end)

    errors = {key: list(value) for key, value in ErrorManager().get_errors().items()}
    return errors
